package ru.kav.sb;
import java.util.Scanner;

/**
 * Основной метод для запуска игры морской бой,пользователь вводит ОДНУ координату и при попадании выводится соотвествующий результат.
 */
public class Game {
        static Scanner scanner = new Scanner(System.in);
        public static void main(String[] args) {
            int a = (int) (Math.random() * 8);
            Ship ship = new Ship();
            ship.setLocation(a);
            System.out.println(ship.toString());
            int count=0;
            String res;
            do {
                System.out.println("Введите координату ");
                int shot = scanner.nextInt();
                count++;
                res = ship.chekShot(shot);
                System.out.println(res);
            } while (!res.equals("Убил!"));
            System.out.println("Победа!");
            System.out.println("Попытки " + count);
        }
}

