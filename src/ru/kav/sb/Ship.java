package ru.kav.sb;
import java.util.ArrayList;

/**
 * Объект класса "Корабль"  хранит информацию о местоположении корабля и  обрабатывает выстрел пользователя.
 * @author *******
 */
public class Ship {
  private ArrayList<String> location = new ArrayList<>();

    /**
     * Метод для присваивания начального положения коробля.
     * @param a координаты корабля.
     */
    public void setLocation(int a) {
        location.add(Integer.toString(a));
        location.add(Integer.toString(a + 1));
        location.add(Integer.toString(a + 2));
    }

    /**
     * Метод для обработки выстрела пользователя,при попадании выводит сообщение пользователю "Попал","Убил" или "Мимо".
     * @param shot выстрел ,который принимает значение введеное пользователем с клавиатуры.
     */
    public String chekShot(int shot) {
            int index = location.indexOf(Integer.toString(shot));
            String result = "Мимо";
            if (index != -1) {
                location.remove(index);
                if (location.isEmpty()) {
                    result = "Убил!";
                } else {
                    result = "Ранил!";
                }
            } return result;
        }



        /**
         * Метод toString() выводит  информацию о текущей позиции корабля.
         * @return возвращает положение корабля.
         */
        @Override
        public String toString() {
            return "Расположение коробля = " + location +
                    '}';
        }
    }

